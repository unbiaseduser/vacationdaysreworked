plugins {
    id("com.android.application")
    kotlin("android")
    id("kotlin-parcelize")
    id("androidx.navigation.safeargs.kotlin")
    id("com.google.devtools.ksp") version "2.1.0-1.0.29"
    id("de.mannodermaus.android-junit5") version "1.10.0.0"
}

ksp {
    arg("room.schemaLocation", "$projectDir/schemas")
}

android {
    namespace = "com.sixtyninefourtwenty.vacationdaysreworked"
    compileSdk = 35

    defaultConfig {
        applicationId = "com.sixtyninefourtwenty.vacationdaysreworked"
        minSdk = 21
        targetSdk = 35
        versionCode = 1
        versionName = "1.0"

        setProperty("archivesBaseName", "VCR v$versionName")

        resourceConfigurations += listOf("en", "vi")

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        testInstrumentationRunnerArguments["runnerBuilder"] = "de.mannodermaus.junit5.AndroidJUnit5Builder"
    }

    signingConfigs {
        create("release") {
            storeFile = findProperty("androidStoreFile")?.let { File(it.toString()) }
            keyAlias = findProperty("androidKeyAlias")?.toString()
            storePassword = findProperty("androidStorePassword")?.toString()
            keyPassword = findProperty("androidKeyPassword")?.toString()
        }
    }

    buildTypes {
        release {
            signingConfig = findProperty("androidStoreFile")?.let { signingConfigs["release"] }
            isMinifyEnabled = true
            proguardFiles += getDefaultProguardFile("proguard-android-optimize.txt")
            proguardFiles += file("proguard-rules.pro")
        }
        debug {
            applicationIdSuffix = ".debug"
            versionNameSuffix = "-debug"
        }
    }
    compileOptions {
        isCoreLibraryDesugaringEnabled = true
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }
    buildFeatures {
        viewBinding = true
    }
    packaging {
        resources {
            excludes += ("META-INF/atomicfu.kotlin_module")
        }
    }
    sourceSets {
        // Adds exported schema location as test app assets.
        getByName("androidTest").assets.srcDir("$projectDir/schemas")
    }

}

dependencies {

    implementation("androidx.core:core-ktx:1.15.0")
    implementation("androidx.appcompat:appcompat:1.7.0")
    implementation("com.google.android.material:material:1.12.0")
    implementation("androidx.constraintlayout:constraintlayout:2.2.0")
    val navigation = "2.8.5"
    implementation("androidx.navigation:navigation-fragment-ktx:$navigation")
    implementation("androidx.navigation:navigation-ui-ktx:$navigation")
    val room = "2.6.1"
    implementation("androidx.room:room-runtime:$room")
    implementation("androidx.room:room-ktx:$room")
    ksp("androidx.room:room-compiler:$room")
    val lifecycle = "2.8.7"
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:$lifecycle")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycle")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:$lifecycle")
    implementation("androidx.preference:preference-ktx:1.2.1")
    implementation("androidx.recyclerview:recyclerview-selection:1.1.0")
    implementation("com.kizitonwose.calendar:view:2.6.0")
    implementation ("com.github.cachapa:ExpandableLayout:2.9.2")
    implementation("com.github.Kennyc1012:MultiStateView:2.2.0")
    implementation("io.coil-kt:coil:2.7.0")
    implementation("com.github.unbiaseduser-github:bottom-sheet-alert-dialog:2.0.2")
    implementation("com.github.unbiaseduser-github:base-fragments:1.2.0")
    implementation("com.github.unbiaseduser-github:custom-preferences-theming-integration:2.1.8")
    testImplementation("junit:junit:4.13.2")
    val junitJupiter = "5.11.3"
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitJupiter")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$junitJupiter")
    androidTestImplementation("androidx.room:room-testing:$room")
    androidTestImplementation("androidx.test.ext:junit:1.2.1")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.6.1")
    androidTestImplementation("androidx.test:runner:1.6.2")
    androidTestImplementation("org.junit.jupiter:junit-jupiter-api:$junitJupiter")
    val junit5Android = "1.6.0"
    androidTestImplementation("de.mannodermaus.junit5:android-test-core:$junit5Android")
    androidTestRuntimeOnly("de.mannodermaus.junit5:android-test-runner:$junit5Android")
    androidTestRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$junitJupiter")
    debugImplementation("androidx.fragment:fragment-testing:1.8.5")
    coreLibraryDesugaring("com.android.tools:desugar_jdk_libs:2.1.3")

}