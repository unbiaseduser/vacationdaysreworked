package com.sixtyninefourtwenty

import androidx.room.testing.MigrationTestHelper
import androidx.test.platform.app.InstrumentationRegistry
import com.sixtyninefourtwenty.vacationdaysreworked.data.db.VacationsDatabase
import org.junit.Before
import org.junit.Rule
import org.junit.Test

private const val TEST_DB = "migration-test"

class VacationsDatabaseMigrationTest {

    @Rule
    lateinit var helper: MigrationTestHelper

    @Before
    fun before() {
        helper = MigrationTestHelper(
            InstrumentationRegistry.getInstrumentation(),
            VacationsDatabase::class.java
        )
    }

    // Version 2 only adds a table. Data in the database is irrelevant.
    @Test
    fun migrate1to2() {
        helper.createDatabase(TEST_DB, 1).use {}
        helper.runMigrationsAndValidate(TEST_DB, 2, true, VacationsDatabase.MIGRATION_1_2).use {}
    }

    // Version 3 only adds an index. Data in the database is irrelevant.
    @Test
    fun migrate2to3() {
        helper.createDatabase(TEST_DB, 2).use {}
        helper.runMigrationsAndValidate(TEST_DB, 3, true, VacationsDatabase.MIGRATION_2_3).use {}
    }

}