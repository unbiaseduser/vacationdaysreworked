package com.sixtyninefourtwenty.vacationdaysreworked

import android.app.Application
import com.sixtyninefourtwenty.vacationdaysreworked.data.db.VacationsDatabase
import com.sixtyninefourtwenty.vacationdaysreworked.data.repository.Preferences
import com.sixtyninefourtwenty.vacationdaysreworked.data.repository.VacationImagesRepository
import com.sixtyninefourtwenty.vacationdaysreworked.data.repository.VacationsRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class MyApplication : Application() {
    private val scope = CoroutineScope(SupervisorJob())
    private val database by lazy { VacationsDatabase.getDatabase(this) }
    val repository by lazy { VacationsRepository(database.vacationsDao(), scope) }
    val vacationImagesRepository by lazy { VacationImagesRepository(database.vacationImagesDao(), scope) }
    val prefs by lazy { Preferences(this) }
}
