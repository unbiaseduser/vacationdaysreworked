package com.sixtyninefourtwenty.vacationdaysreworked.dialogs.addeditvacation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.sixtyninefourtwenty.vacationdaysreworked.MyApplication
import com.sixtyninefourtwenty.vacationdaysreworked.data.repository.VacationsRepository
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async

class AddEditVacationViewModel(
    private val vacationsRepository: VacationsRepository
) : ViewModel() {

    fun checkIfVacationExistsAsync(name: String): Deferred<Boolean> =
        viewModelScope.async { vacationsRepository.checkIfVacationExists(name) }

    companion object {
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val app = get(ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY) as MyApplication
                AddEditVacationViewModel(app.repository)
            }
        }
    }

}