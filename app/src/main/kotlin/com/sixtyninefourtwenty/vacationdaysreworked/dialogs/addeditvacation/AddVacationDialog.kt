package com.sixtyninefourtwenty.vacationdaysreworked.dialogs.addeditvacation

import android.os.Bundle
import androidx.core.os.BundleCompat
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LifecycleOwner
import com.sixtyninefourtwenty.vacationdaysreworked.R
import com.sixtyninefourtwenty.vacationdaysreworked.data.Vacation

class AddVacationDialog : AbstractAddEditVacationDialog() {

    companion object {
        private const val ADD_VACATION_REQUEST_KEY = "add_vacation_request"
        private const val VACATION_KEY = "vacation"
        fun registerCallback(
            activity: FragmentActivity,
            lifecycleOwner: LifecycleOwner,
            callback: (Vacation) -> Unit
        ) {
            activity.supportFragmentManager.setFragmentResultListener(
                ADD_VACATION_REQUEST_KEY,
                lifecycleOwner
            ) { _, result ->
                callback(BundleCompat.getParcelable(result, VACATION_KEY, Vacation::class.java)!!)
            }
        }
    }

    override val existingVacation: Vacation? = null

    override val title: Int = R.string.add_vacation

    override val subclassFragmentManager: FragmentManager
        get() = childFragmentManager

    override fun onVacationCreated(newVacation: Vacation) {
        requireActivity().supportFragmentManager.setFragmentResult(
            ADD_VACATION_REQUEST_KEY,
            Bundle().apply {
                putParcelable(VACATION_KEY, newVacation)
            }
        )
        dismiss()
    }

}