package com.sixtyninefourtwenty.vacationdaysreworked.dialogs.addeditvacation

import android.os.Bundle
import androidx.core.os.BundleCompat
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.fragment.navArgs
import com.sixtyninefourtwenty.vacationdaysreworked.R
import com.sixtyninefourtwenty.vacationdaysreworked.data.Vacation

class EditVacationDialog : AbstractAddEditVacationDialog() {

    companion object {
        private const val EDIT_VACATION_REQUEST_KEY = "edit_vacation_request"
        private const val NEW_VACATION_KEY = "new_vacation"
        private const val OLD_VACATION_KEY = "old_vacation"
        fun registerCallback(
            activity: FragmentActivity,
            lifecycleOwner: LifecycleOwner,
            callback: (old: Vacation, new: Vacation) -> Unit
        ) {
            activity.supportFragmentManager.setFragmentResultListener(
                EDIT_VACATION_REQUEST_KEY,
                lifecycleOwner
            ) { _, result ->
                callback(
                    BundleCompat.getParcelable(result, OLD_VACATION_KEY, Vacation::class.java)!!,
                    BundleCompat.getParcelable(result, NEW_VACATION_KEY, Vacation::class.java)!!
                )
            }
        }
    }

    private val args: EditVacationDialogArgs by navArgs()

    override val existingVacation: Vacation
        get() = args.vacation

    override val title: Int = R.string.edit_vacation

    override val subclassFragmentManager: FragmentManager
        get() = childFragmentManager

    override fun onVacationCreated(newVacation: Vacation) {
        requireActivity().supportFragmentManager.setFragmentResult(
            EDIT_VACATION_REQUEST_KEY,
            Bundle().apply {
                putParcelable(OLD_VACATION_KEY, args.vacation)
                putParcelable(NEW_VACATION_KEY, newVacation)
            }
        )
        dismiss()
    }

}