package com.sixtyninefourtwenty.vacationdaysreworked.dialogs.addeditvacationimage

import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import coil.load
import com.sixtyninefourtwenty.bottomsheetalertdialog.BottomSheetAlertDialogFragmentViewBuilder
import com.sixtyninefourtwenty.bottomsheetalertdialog.DialogButtonProperties
import com.sixtyninefourtwenty.bottomsheetalertdialog.misc.ViewBindingBottomSheetAlertDialogFragment
import com.sixtyninefourtwenty.bottomsheetalertdialog.misc.createBottomSheetAlertDialog
import com.sixtyninefourtwenty.vacationdaysreworked.R
import com.sixtyninefourtwenty.vacationdaysreworked.data.Vacation
import com.sixtyninefourtwenty.vacationdaysreworked.data.VacationImage
import com.sixtyninefourtwenty.vacationdaysreworked.databinding.DialogAddEditVacationImageBinding
import com.sixtyninefourtwenty.vacationdaysreworked.utils.getInput
import kotlinx.coroutines.launch

abstract class AbstractAddEditVacationImageDialog : ViewBindingBottomSheetAlertDialogFragment<DialogAddEditVacationImageBinding>(DialogAddEditVacationImageBinding::inflate) {

    private val viewModel: AddEditVacationImageViewModel by viewModels { AddEditVacationImageViewModel.Factory }

    protected fun handleImage(uri: Uri) {
        lifecycleOwner.lifecycleScope.launch {
            val exists = viewModel.checkIfVacationImageExistsAsync(uri).await()
            if (exists) {
                Toast.makeText(requireContext(), R.string.image_already_exists_error, Toast.LENGTH_SHORT).show()
            } else {
                viewModel.uri = uri
            }
        }
    }

    abstract val lifecycleOwner: LifecycleOwner

    abstract fun onChooseImageButtonClick()

    abstract val associatedVacation: Vacation

    abstract val existingImage: VacationImage?

    @get:StringRes
    abstract val title: Int

    abstract fun onImageCreated(image: VacationImage)

    override fun createDialog(binding: DialogAddEditVacationImageBinding): BottomSheetAlertDialogFragmentViewBuilder {
        return createBottomSheetAlertDialog(
            view = binding.root,
            titleText = getString(title),
            positiveButtonProperties = DialogButtonProperties(
                text = getString(android.R.string.ok),
                listener = {
                    val image = existingImage
                    val viewModelUri = viewModel.uri
                    if (image == null && viewModel.uri == null) {
                        Toast.makeText(requireContext(), R.string.image_not_chosen, Toast.LENGTH_SHORT).show()
                    } else {
                        onImageCreated(
                            image?.copy(title = binding.titleInput.getInput())
                                ?: VacationImage(
                                    uri = viewModelUri!!,
                                    vacationName = associatedVacation.name,
                                    title = binding.titleInput.getInput()
                                )
                        )
                    }

                },
                dismissAfterClick = false
            ),
            negativeButtonProperties = DialogButtonProperties(
                text = getString(android.R.string.cancel)
            )
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val binding = requireBinding()
        val image = existingImage
        if (image != null) {
            binding.imageTitle.visibility = View.GONE
            binding.image.visibility = View.GONE
            binding.chooseImage.visibility = View.GONE
            binding.titleInput.setText(image.title)
        } else {
            viewModel.uriLiveData.observe(lifecycleOwner) {
                binding.image.load(it)
            }
            binding.chooseImage.setOnClickListener {
                onChooseImageButtonClick()
            }
        }
    }

}