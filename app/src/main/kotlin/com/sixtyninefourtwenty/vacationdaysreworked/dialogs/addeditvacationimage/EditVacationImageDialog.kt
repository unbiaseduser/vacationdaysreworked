package com.sixtyninefourtwenty.vacationdaysreworked.dialogs.addeditvacationimage

import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.os.BundleCompat
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.fragment.navArgs
import com.sixtyninefourtwenty.vacationdaysreworked.R
import com.sixtyninefourtwenty.vacationdaysreworked.data.Vacation
import com.sixtyninefourtwenty.vacationdaysreworked.data.VacationImage

class EditVacationImageDialog : AbstractAddEditVacationImageDialog() {

    companion object {
        private const val EDIT_IMAGE_REQUEST_KEY = "edit_img_request"
        private const val EXISTING_IMAGE_KEY = "existing_img"
        private const val NEW_IMAGE_KEY = "new_img"
        fun registerCallback(
            activity: FragmentActivity,
            lifecycleOwner: LifecycleOwner,
            callback: (old: VacationImage, new: VacationImage) -> Unit
        ) {
            activity.supportFragmentManager.setFragmentResultListener(
                EDIT_IMAGE_REQUEST_KEY,
                lifecycleOwner
            ) { _, result ->
                callback(
                    BundleCompat.getParcelable(result, EXISTING_IMAGE_KEY, VacationImage::class.java)!!,
                    BundleCompat.getParcelable(result, NEW_IMAGE_KEY, VacationImage::class.java)!!
                )
            }
        }
    }

    private val args: EditVacationImageDialogArgs by navArgs()

    private val launcher = registerForActivityResult(ActivityResultContracts.OpenDocument()) {
        if (it != null) {
            handleImage(it)
        }
    }

    override fun onChooseImageButtonClick() {
        launcher.launch(arrayOf("image/*"))
    }

    override val lifecycleOwner: LifecycleOwner
        get() = viewLifecycleOwner

    override val associatedVacation: Vacation
        get() = args.vacation

    override val existingImage: VacationImage
        get() = args.vacationImage

    override val title: Int
        get() = R.string.edit_image

    override fun onImageCreated(image: VacationImage) {
        requireActivity().supportFragmentManager.setFragmentResult(
            EDIT_IMAGE_REQUEST_KEY,
            Bundle().apply {
                putParcelable(EXISTING_IMAGE_KEY, args.vacationImage)
                putParcelable(NEW_IMAGE_KEY, image)
            }
        )
        dismiss()
    }

}