package com.sixtyninefourtwenty.vacationdaysreworked.dialogs.addeditvacationimage

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.sixtyninefourtwenty.vacationdaysreworked.MyApplication
import com.sixtyninefourtwenty.vacationdaysreworked.data.repository.VacationImagesRepository
import kotlinx.coroutines.async

class AddEditVacationImageViewModel(
    savedStateHandle: SavedStateHandle,
    private val repository: VacationImagesRepository
) : ViewModel() {

    private val _uri: MutableLiveData<Uri?> = savedStateHandle.getLiveData("uri")

    val uriLiveData: LiveData<Uri?> get() = _uri

    var uri: Uri? by _uri::value

    fun checkIfVacationImageExistsAsync(uri: Uri) = viewModelScope.async {
        repository.checkIfVacationImageExists(uri)
    }

    companion object {
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val app = get(ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY) as MyApplication
                AddEditVacationImageViewModel(createSavedStateHandle(), app.vacationImagesRepository)
            }
        }
    }

}