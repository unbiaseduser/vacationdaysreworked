package com.sixtyninefourtwenty.vacationdaysreworked.dialogs.addeditvacation

import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.material.datepicker.MaterialDatePicker
import com.sixtyninefourtwenty.bottomsheetalertdialog.BottomSheetAlertDialogFragmentViewBuilder
import com.sixtyninefourtwenty.bottomsheetalertdialog.DialogButtonProperties
import com.sixtyninefourtwenty.bottomsheetalertdialog.misc.ViewBindingBottomSheetAlertDialogFragment
import com.sixtyninefourtwenty.bottomsheetalertdialog.misc.createBottomSheetAlertDialog
import com.sixtyninefourtwenty.vacationdaysreworked.R
import com.sixtyninefourtwenty.vacationdaysreworked.data.Vacation
import com.sixtyninefourtwenty.vacationdaysreworked.databinding.DialogAddEditVacationBinding
import com.sixtyninefourtwenty.vacationdaysreworked.utils.getInput
import com.sixtyninefourtwenty.vacationdaysreworked.utils.isBlank
import kotlinx.coroutines.launch

abstract class AbstractAddEditVacationDialog : ViewBindingBottomSheetAlertDialogFragment<DialogAddEditVacationBinding>(DialogAddEditVacationBinding::inflate) {

    abstract val existingVacation: Vacation?

    @get:StringRes
    abstract val title: Int

    abstract fun onVacationCreated(newVacation: Vacation)

    abstract val subclassFragmentManager: FragmentManager

    private val addEditVacationViewModel: AddEditVacationViewModel by viewModels { AddEditVacationViewModel.Factory }

    override fun createDialog(binding: DialogAddEditVacationBinding): BottomSheetAlertDialogFragmentViewBuilder {
        fun openSingleDatePicker() {
            MaterialDatePicker.Builder.datePicker().build().apply {
                addOnPositiveButtonClickListener {
                    onVacationCreated(
                        Vacation(
                            name = existingVacation?.name ?: binding.nameInput.getInput(),
                            description = binding.descInput.getInput(),
                            isSickDay = binding.isSick.isChecked,
                            timeFrom = it,
                            timeTo = it
                        )
                    )
                }
                show(subclassFragmentManager, null)
            }
        }

        fun openDateRangePicker() {
            MaterialDatePicker.Builder.dateRangePicker().build().apply {
                addOnPositiveButtonClickListener {
                    onVacationCreated(
                        Vacation(
                            name = existingVacation?.name ?: binding.nameInput.getInput(),
                            description = binding.descInput.getInput(),
                            isSickDay = binding.isSick.isChecked,
                            timeFrom = it.first,
                            timeTo = it.second
                        )
                    )
                }
                show(subclassFragmentManager, null)
            }
        }
        return createBottomSheetAlertDialog(
            view = binding.root,
            titleText = getString(title),
            positiveButtonProperties = DialogButtonProperties(
                text = getString(android.R.string.ok),
                listener = {
                    fun isNoErrorsPresent(): Boolean {
                        return binding.nameInputLayout.error == null
                    }
                    fun handleAddingVacation() {
                        when {
                            binding.singleDate.isChecked -> openSingleDatePicker()
                            binding.dateRange.isChecked -> openDateRangePicker()
                            binding.dontUpdateDates.isChecked -> {
                                //If this one is checked, that means we're editing a vacation.
                                val vacation = existingVacation!!
                                onVacationCreated(
                                    Vacation(
                                        name = vacation.name,
                                        description = binding.descInput.getInput(),
                                        isSickDay = binding.isSick.isChecked,
                                        timeFrom = vacation.timeFrom,
                                        timeTo = vacation.timeTo
                                    )
                                )
                            }
                        }
                    }

                    if (existingVacation == null) {
                        if (binding.nameInput.isBlank()) {
                            binding.nameInputLayout.error = getString(R.string.empty_name_error)
                        } else {
                            viewLifecycleOwner.lifecycleScope.launch {
                                val isVacationPresent = addEditVacationViewModel.checkIfVacationExistsAsync(binding.nameInput.getInput()).await()
                                if (isVacationPresent) {
                                    binding.nameInputLayout.error = getString(R.string.vacation_already_exists_error)
                                } else {
                                    binding.nameInputLayout.error = null
                                }
                                if (isNoErrorsPresent()) {
                                    handleAddingVacation()
                                }
                            }
                        }
                    } else {
                        if (isNoErrorsPresent()) {
                            handleAddingVacation()
                        }
                    }
                },
                dismissAfterClick = false
            ),
            negativeButtonProperties = DialogButtonProperties(text = getString(android.R.string.cancel))
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val binding = requireBinding()
        val vacation = existingVacation
        if (vacation != null) {
            binding.descInput.setText(vacation.description)
            with(binding.nameInput) {
                setText(vacation.name)
                visibility = View.GONE
            }
            binding.isSick.isChecked = vacation.isSickDay
            with(binding.dontUpdateDates) {
                isChecked = true
                visibility = View.VISIBLE
            }
        } else {
            binding.dontUpdateDates.visibility = View.GONE
        }
    }

}