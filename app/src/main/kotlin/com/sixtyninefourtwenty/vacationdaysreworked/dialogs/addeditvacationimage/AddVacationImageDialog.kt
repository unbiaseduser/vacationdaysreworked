package com.sixtyninefourtwenty.vacationdaysreworked.dialogs.addeditvacationimage

import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.os.BundleCompat
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.fragment.navArgs
import com.sixtyninefourtwenty.vacationdaysreworked.R
import com.sixtyninefourtwenty.vacationdaysreworked.data.Vacation
import com.sixtyninefourtwenty.vacationdaysreworked.data.VacationImage

class AddVacationImageDialog : AbstractAddEditVacationImageDialog() {

    companion object {
        private const val ADD_IMAGE_REQUEST_KEY = "add_img_request"
        private const val NEW_IMAGE_KEY = "new_img"
        fun registerCallback(
            activity: FragmentActivity,
            lifecycleOwner: LifecycleOwner,
            callback: (VacationImage) -> Unit
        ) {
            activity.supportFragmentManager.setFragmentResultListener(
                ADD_IMAGE_REQUEST_KEY,
                lifecycleOwner
            ) { _, result ->
                callback(BundleCompat.getParcelable(result, NEW_IMAGE_KEY, VacationImage::class.java)!!)
            }
        }
    }

    private val args: AddVacationImageDialogArgs by navArgs()

    private val launcher = registerForActivityResult(ActivityResultContracts.OpenDocument()) {
        if (it != null) {
            handleImage(it)
        }
    }

    override fun onChooseImageButtonClick() {
        launcher.launch(arrayOf("image/*"))
    }

    override val lifecycleOwner: LifecycleOwner
        get() = viewLifecycleOwner

    override val associatedVacation: Vacation
        get() = args.vacation

    override val existingImage: VacationImage?
        get() = null

    override val title: Int
        get() = R.string.add_image

    override fun onImageCreated(image: VacationImage) {
        requireActivity().supportFragmentManager.setFragmentResult(
            ADD_IMAGE_REQUEST_KEY,
            Bundle().apply {
                putParcelable(NEW_IMAGE_KEY, image)
            }
        )
        dismiss()
    }

}