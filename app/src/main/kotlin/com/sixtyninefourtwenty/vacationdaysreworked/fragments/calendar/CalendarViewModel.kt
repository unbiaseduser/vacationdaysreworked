package com.sixtyninefourtwenty.vacationdaysreworked.fragments.calendar

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.sixtyninefourtwenty.vacationdaysreworked.MyApplication
import com.sixtyninefourtwenty.vacationdaysreworked.data.repository.VacationsRepository

class CalendarViewModel(
    vacationsRepository: VacationsRepository
) : ViewModel() {

    val allVacationsByTimeFromAscending = vacationsRepository.allVacationsByTimeFromAscending

    companion object {
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val app = get(ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY) as MyApplication
                CalendarViewModel(app.repository)
            }
        }
    }

}