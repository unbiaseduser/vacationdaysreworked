package com.sixtyninefourtwenty.vacationdaysreworked.fragments

import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.core.os.BundleCompat
import androidx.core.view.get
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import coil.load
import com.sixtyninefourtwenty.basefragments.ViewFragment

class FullImageFragment : ViewFragment<FrameLayout>(::FrameLayout) {

    companion object {
        private const val ERROR_REQUEST_KEY = "error_request"
        private const val URI_KEY = "uri"
        fun registerErrorCallback(
            activity: FragmentActivity,
            lifecycleOwner: LifecycleOwner,
            callback: (Uri) -> Unit
        ) {
            activity.supportFragmentManager.setFragmentResultListener(
                ERROR_REQUEST_KEY,
                lifecycleOwner
            ) { _, result ->
                callback(BundleCompat.getParcelable(result, URI_KEY, Uri::class.java)!!)
            }
        }
    }

    private val args: FullImageFragmentArgs by navArgs<FullImageFragmentArgs>()

    override fun onCreateView(
        view: FrameLayout,
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        view.addView(ImageView(requireContext()).apply {
            layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
                gravity = Gravity.CENTER
            }
            scaleType = ImageView.ScaleType.CENTER_CROP
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (requireTypedView()[0] as ImageView).load(args.uri) {
            listener(
                onError = { _, _ ->
                    requireActivity().supportFragmentManager.setFragmentResult(
                        ERROR_REQUEST_KEY,
                        Bundle().apply {
                            putParcelable(URI_KEY, args.uri)
                        }
                    )
                    findNavController().popBackStack()
                }
            )
        }
    }

}