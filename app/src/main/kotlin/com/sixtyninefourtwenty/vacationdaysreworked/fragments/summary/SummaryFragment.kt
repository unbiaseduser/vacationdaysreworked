package com.sixtyninefourtwenty.vacationdaysreworked.fragments.summary

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sixtyninefourtwenty.basefragments.ViewBindingFragment
import com.sixtyninefourtwenty.vacationdaysreworked.R
import com.sixtyninefourtwenty.vacationdaysreworked.data.Vacation
import com.sixtyninefourtwenty.vacationdaysreworked.data.VacationDaysPerMonth
import com.sixtyninefourtwenty.vacationdaysreworked.databinding.FragmentSummaryBinding
import com.sixtyninefourtwenty.vacationdaysreworked.databinding.ListItemVacationPerMonthBinding
import com.sixtyninefourtwenty.vacationdaysreworked.utils.myApplication
import com.sixtyninefourtwenty.vacationdaysreworked.utils.obtainDeviceLocale
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.Month
import java.time.format.TextStyle
import kotlin.math.roundToInt

class SummaryFragment : ViewBindingFragment<FragmentSummaryBinding>(FragmentSummaryBinding::inflate) {

    private var trackSickDays = false
    private val summaryViewModel: SummaryViewModel by viewModels { SummaryViewModel.Factory }
    private val adapter = VacationDaysPerMonthAdapter()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val binding = requireBinding()
        with(binding.list) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = this@SummaryFragment.adapter
        }
        trackSickDays = myApplication.prefs.trackSickDays
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                summaryViewModel.allVacationsByTimeFromAscending.collect { list ->
                    binding.yearPicker.adapter = ArrayAdapter(
                        requireContext(),
                        com.sixtyninefourtwenty.theming.R.layout.spinner_item_m3,
                        getYearsVacationsAreIn(list)
                    )
                    binding.yearPicker.onItemSelectedListener =
                        object : AdapterView.OnItemSelectedListener {
                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View?,
                                position: Int,
                                id: Long
                            ) {
                                val year = parent!!.selectedItem as Int
                                val numOfVacationDaysForYear = getNumOfVacationDaysForYear(year, list)
                                binding.summaryYear.text = getString(
                                    R.string.summary_year,
                                    year,
                                    requireContext().resources.getQuantityString(
                                        R.plurals.vacation_days,
                                        numOfVacationDaysForYear,
                                        numOfVacationDaysForYear
                                    )
                                )
                                adapter.submitList(getVacationDaysPerMonth(list, year))
                                adapter.setYear(year)
                            }

                            override fun onNothingSelected(parent: AdapterView<*>?) {/*unused*/
                            }
                        }
                }
            }
        }
    }

    private fun getYearsVacationsAreIn(vacations: List<Vacation>): List<Int> =
        vacations.flatMap { it.dates }.map { it.year }.distinct()

    private fun getNumOfVacationDaysForMonth(month: Month, year: Int, vacations: List<Vacation>): Int {
        var copyOfVacations = vacations
        if (!trackSickDays) {
            copyOfVacations = copyOfVacations.filter { !it.isSickDay }
        }
        return copyOfVacations.flatMap { it.dates }
            .filter { it.month == month && it.year == year }
            .distinct()
            .size
    }

    private fun getVacationDaysPerMonth(vacations: List<Vacation>, year: Int): List<VacationDaysPerMonth> {
        val result = mutableListOf<VacationDaysPerMonth>()
        var copyOfVacations = vacations
        if (!trackSickDays) {
            copyOfVacations = copyOfVacations.filter { !it.isSickDay }
        }
        val datesInYear = copyOfVacations.flatMap { it.dates }
            .filter { it.year == year }
            .distinct()
        for (month in Month.entries) {
            val numOfDates = datesInYear.filter { it.month == month }.size
            if (numOfDates > 0) {
                result.add(VacationDaysPerMonth(month, numOfDates))
            }
        }
        return result
    }

    private fun getNumOfVacationDaysForYear(year: Int, vacations: List<Vacation>): Int {
        var total = 0
        for (month in Month.entries)
            total += getNumOfVacationDaysForMonth(month, year, vacations)
        return total
    }

    private class VacationDaysPerMonthAdapter : ListAdapter<VacationDaysPerMonth, VacationDaysPerMonthAdapter.ViewHolder>(DIFFER) {

        private var isLeapYear = false

        @SuppressLint("NotifyDataSetChanged")
        fun setYear(year: Int) {
            isLeapYear = LocalDate.of(year, 1, 1).isLeapYear
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val binding = ListItemVacationPerMonthBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return ViewHolder(binding)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = getItem(position)
            val context = holder.binding.root.context
            val daysInMonth = item.month.length(isLeapYear)
            holder.binding.month.text = item.month.getDisplayName(TextStyle.FULL, obtainDeviceLocale(context))
            holder.binding.summary.text = context.getString(
                R.string.summary_month,
                context.resources.getQuantityString(
                    R.plurals.vacation_days,
                    item.numOfVacationDaysForMonth,
                    item.numOfVacationDaysForMonth
                ),
                daysInMonth
            )
            holder.binding.bar.progress =
                ((item.numOfVacationDaysForMonth / daysInMonth.toFloat()) * 100).roundToInt()
        }

        companion object {
            private val DIFFER = object : DiffUtil.ItemCallback<VacationDaysPerMonth>() {
                override fun areItemsTheSame(
                    oldItem: VacationDaysPerMonth,
                    newItem: VacationDaysPerMonth
                ): Boolean = oldItem.month == newItem.month

                override fun areContentsTheSame(
                    oldItem: VacationDaysPerMonth,
                    newItem: VacationDaysPerMonth
                ): Boolean = oldItem == newItem

            }
        }

        private class ViewHolder(val binding: ListItemVacationPerMonthBinding) : RecyclerView.ViewHolder(binding.root) {

        }
    }

}