package com.sixtyninefourtwenty.vacationdaysreworked.fragments.vacationimages

import android.content.ContentResolver
import android.content.Intent
import android.net.Uri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory.Companion.APPLICATION_KEY
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.sixtyninefourtwenty.vacationdaysreworked.MyApplication
import com.sixtyninefourtwenty.vacationdaysreworked.data.Vacation
import com.sixtyninefourtwenty.vacationdaysreworked.data.VacationImage
import com.sixtyninefourtwenty.vacationdaysreworked.data.repository.VacationImagesRepository
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class VacationImagesViewModel(private val vacationsRepository: VacationImagesRepository) : ViewModel() {

    fun insertVacationImages(
        vararg images: VacationImage,
        contentResolver: ContentResolver
    ) = viewModelScope.launch {
        images.forEach {
            contentResolver.takePersistableUriPermission(it.uri, Intent.FLAG_GRANT_READ_URI_PERMISSION)
        }
        vacationsRepository.insertVacationImages(*images)
    }

    fun updateVacationImages(vararg images: VacationImage) = viewModelScope.launch {
        vacationsRepository.updateVacationImages(*images)
    }

    fun deleteVacationImages(
        vararg images: VacationImage,
        contentResolver: ContentResolver
    ) = viewModelScope.launch {
        images.forEach {
            contentResolver.releasePersistableUriPermission(it.uri, Intent.FLAG_GRANT_READ_URI_PERMISSION)
        }
        vacationsRepository.deleteVacationImages(*images)
    }

    fun getImagesForVacation(vacation: Vacation) = vacationsRepository.getImagesForVacation(vacation)

    fun findImageByUriAsync(uri: Uri) = viewModelScope.async {
        vacationsRepository.findImageByUri(uri)
    }

    companion object {
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val repo = (get(APPLICATION_KEY) as MyApplication).vacationImagesRepository
                VacationImagesViewModel(repo)
            }
        }
    }

}