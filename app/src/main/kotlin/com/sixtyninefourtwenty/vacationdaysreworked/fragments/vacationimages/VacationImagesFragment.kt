package com.sixtyninefourtwenty.vacationdaysreworked.fragments.vacationimages

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ActionMode
import androidx.appcompat.widget.PopupMenu
import androidx.core.view.MenuProvider
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.ItemDetailsLookup.ItemDetails
import androidx.recyclerview.selection.ItemKeyProvider
import androidx.recyclerview.selection.SelectionPredicates
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.selection.SelectionTracker.SelectionObserver
import androidx.recyclerview.selection.StorageStrategy
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import coil.load
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.kennyc.view.MultiStateView
import com.sixtyninefourtwenty.basefragments.ViewBindingFragment
import com.sixtyninefourtwenty.vacationdaysreworked.R
import com.sixtyninefourtwenty.vacationdaysreworked.data.VacationImage
import com.sixtyninefourtwenty.vacationdaysreworked.databinding.FragmentVacationImagesBinding
import com.sixtyninefourtwenty.vacationdaysreworked.databinding.ListItemVacationImageBinding
import com.sixtyninefourtwenty.vacationdaysreworked.dialogs.addeditvacationimage.AddVacationImageDialog
import com.sixtyninefourtwenty.vacationdaysreworked.dialogs.addeditvacationimage.EditVacationImageDialog
import com.sixtyninefourtwenty.vacationdaysreworked.fragments.FullImageFragment
import com.sixtyninefourtwenty.vacationdaysreworked.utils.DisposableHandleContainer
import com.sixtyninefourtwenty.vacationdaysreworked.utils.navigate
import kotlinx.coroutines.launch

class VacationImagesFragment : ViewBindingFragment<FragmentVacationImagesBinding>(FragmentVacationImagesBinding::inflate) {

    private lateinit var disposableHandleContainer: DisposableHandleContainer
    private lateinit var selectionTracker: SelectionTracker<String>
    private val viewModel: VacationImagesViewModel by viewModels { VacationImagesViewModel.Factory }
    private val args: VacationImagesFragmentArgs by navArgs()
    private var actionMode: ActionMode? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val binding = requireBinding()
        disposableHandleContainer = DisposableHandleContainer(viewLifecycleOwner)
        val adapter = VacationImageAdapter(
            onItemClick = {
                navigate(VacationImagesFragmentDirections.actionVacationImagesFragmentToFullImageFragment(it.uri))
            },
            onItemLongClick = { _, _ -> },
            onMenuIconClick = { item, v ->
                PopupMenu(requireContext(), v).apply {
                    with(menu) {
                        add(R.string.edit).setOnMenuItemClickListener {
                            navigate(VacationImagesFragmentDirections.actionVacationImagesFragmentToEditVacationImageDialog(args.vacation, item))
                            true
                        }
                        add(R.string.delete).setOnMenuItemClickListener {
                            MaterialAlertDialogBuilder(requireContext())
                                .setTitle(R.string.delete)
                                .setMessage(R.string.delete_image_confirmation)
                                .setPositiveButton(android.R.string.ok) { _, _ ->
                                    val job = viewModel.deleteVacationImages(
                                        item,
                                        contentResolver = requireContext().contentResolver
                                    )
                                    disposableHandleContainer.add(job.invokeOnCompletion {
                                        requireActivity().runOnUiThread {
                                            Toast.makeText(requireContext(), R.string.delete_successful, Toast.LENGTH_SHORT).show()
                                        }
                                    })
                                }
                                .setNegativeButton(android.R.string.cancel, null)
                                .show()
                            true
                        }

                    }
                }.show()
            }
        )

        with(binding.list) {
            layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
            this.adapter = adapter
        }

        selectionTracker = SelectionTracker.Builder(
            "selected_images",
            binding.list,
            adapter.KeyProvider(),
            VacationImageAdapter.DetailsLookup(binding.list),
            StorageStrategy.createStringStorage()
        ).withSelectionPredicate(SelectionPredicates.createSelectAnything()).build().also { tracker ->
            tracker.addObserver(object : SelectionObserver<String>() {
                override fun onSelectionChanged() {
                    val selection = tracker.selection
                    if (selection.isEmpty) {
                        actionMode?.finish()
                    } else {
                        val a = actionMode
                        if (a == null) {
                            actionMode = (requireActivity() as AppCompatActivity).startSupportActionMode(object : ActionMode.Callback {
                                var delete: MenuItem? = null

                                override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
                                    delete = menu.add(R.string.delete).apply {
                                        setIcon(R.drawable.delete)
                                        setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)
                                    }
                                    return true
                                }

                                override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
                                    mode.title = resources.getQuantityString(R.plurals.images, selection.size(), selection.size())
                                    return true
                                }

                                override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
                                    if (item == delete) {
                                        MaterialAlertDialogBuilder(requireContext())
                                            .setTitle(R.string.delete)
                                            .setMessage(getString(R.string.delete_images_confirmation, resources.getQuantityString(R.plurals.images, selection.size(), selection.size())))
                                            .setPositiveButton(android.R.string.ok) { _, _ ->
                                                val job = viewModel.deleteVacationImages(
                                                    *adapter.getSelectedItems().toTypedArray(),
                                                    contentResolver = requireContext().contentResolver
                                                )
                                                disposableHandleContainer.add(job.invokeOnCompletion {
                                                    requireActivity().runOnUiThread {
                                                        Toast.makeText(requireContext(), R.string.delete_successful, Toast.LENGTH_SHORT).show()
                                                    }
                                                })
                                            }
                                            .setNegativeButton(android.R.string.cancel, null)
                                            .show()
                                        return true
                                    }
                                    return false
                                }

                                override fun onDestroyActionMode(mode: ActionMode) {
                                    tracker.clearSelection()
                                    actionMode = null
                                }
                            })
                        } else {
                            a.invalidate()
                        }
                    }
                }
            })
            tracker.onRestoreInstanceState(savedInstanceState)
            adapter.selectionTracker = tracker
        }

        binding.add.setOnClickListener {
            navigate(VacationImagesFragmentDirections.actionVacationImagesFragmentToAddVacationImageDialog(args.vacation))
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.getImagesForVacation(args.vacation).collect {
                    adapter.submitList(it)
                    if (it.isEmpty()) {
                        binding.state.viewState = MultiStateView.ViewState.EMPTY
                    } else {
                        binding.state.viewState = MultiStateView.ViewState.CONTENT
                    }
                }
            }
        }

        AddVacationImageDialog.registerCallback(
            requireActivity(),
            viewLifecycleOwner
        ) {
            viewModel.insertVacationImages(it, contentResolver = requireContext().contentResolver)
        }

        EditVacationImageDialog.registerCallback(
            requireActivity(),
            viewLifecycleOwner
        ) { _, new ->
            viewModel.updateVacationImages(new)
        }

        FullImageFragment.registerErrorCallback(
            requireActivity(),
            viewLifecycleOwner
        ) { uri ->
            viewLifecycleOwner.lifecycleScope.launch {
                val existingImage = viewModel.findImageByUriAsync(uri).await()
                if (existingImage != null) {
                    val title = existingImage.title
                    val msg = if (title.isBlank()) {
                        getString(
                            R.string.error_loading_image_message_base,
                            getString(R.string.error_loading_image_message_prefix_without_title)
                        )
                    } else {
                        getString(
                            R.string.error_loading_image_message_base,
                            getString(
                                R.string.error_loading_image_message_prefix_with_title,
                                title
                            )
                        )
                    }
                    MaterialAlertDialogBuilder(requireContext())
                        .setTitle(R.string.error_loading_image)
                        .setMessage(msg)
                        .setPositiveButton(R.string.copy_title_to_clipboard) { _, _ ->
                            val clipboardManager = requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                            clipboardManager.setPrimaryClip(ClipData.newPlainText("vacation_image_title", title))
                            Toast.makeText(requireContext(), R.string.copied_to_clipboard, Toast.LENGTH_SHORT).show()
                        }
                        .setNegativeButton(android.R.string.cancel, null)
                        .show()
                } else {
                    Log.e(javaClass.simpleName, "Wait, what? What do you mean by \"there's no image with uri $uri\"?")
                }
            }
        }

        requireActivity().addMenuProvider(object : MenuProvider {

            private lateinit var disclaimer: MenuItem

            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                disclaimer = menu.add(R.string.disclaimer)
                    .setIcon(R.drawable.exclamation).apply {
                        setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)
                    }
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                if (menuItem == disclaimer) {
                    MaterialAlertDialogBuilder(requireContext())
                        .setTitle(R.string.disclaimer)
                        .setMessage(R.string.vacation_images_disclaimer)
                        .setPositiveButton(android.R.string.ok, null)
                        .show()
                    return true
                }
                return false
            }
        }, viewLifecycleOwner)

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (::selectionTracker.isInitialized) {
            selectionTracker.onSaveInstanceState(outState)
        }
    }

    private class VacationImageAdapter(
        private val onItemClick: (VacationImage) -> Unit,
        private val onItemLongClick: (VacationImage, View) -> Unit,
        private val onMenuIconClick: (VacationImage, View) -> Unit
    ) : ListAdapter<VacationImage, VacationImageAdapter.ViewHolder>(DIFFER) {

        companion object {
            private val DIFFER = object : DiffUtil.ItemCallback<VacationImage>() {
                override fun areItemsTheSame(
                    oldItem: VacationImage,
                    newItem: VacationImage
                ): Boolean {
                    return oldItem.uri == newItem.uri
                }

                override fun areContentsTheSame(
                    oldItem: VacationImage,
                    newItem: VacationImage
                ): Boolean {
                    return oldItem.uri == newItem.uri && oldItem.title == newItem.title
                }

            }
        }

        lateinit var selectionTracker: SelectionTracker<String>

        fun getSelectedItems(): List<VacationImage> = selectionTracker.selection.map { uriString ->
            currentList.first { it.uri.toString() == uriString }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(
                ListItemVacationImageBinding.inflate(LayoutInflater.from(parent.context), parent, false),
                { onItemClick(getItem(it)) },
                { pos, v -> onItemLongClick(getItem(pos), v) },
                { pos, v -> onMenuIconClick(getItem(pos), v) }
            )
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = getItem(position)
            holder.binding.image.load(item.uri) {
                error(R.drawable.broken_image)
            }
            holder.binding.title.text = item.title
            holder.binding.title.visibility = if (item.title.isBlank()) View.GONE else View.VISIBLE
            holder.binding.root.isChecked = selectionTracker.isSelected(item.uri.toString())
        }

        class DetailsLookup(private val recyclerView: RecyclerView) : ItemDetailsLookup<String>() {
            override fun getItemDetails(e: MotionEvent): ItemDetails<String>? {
                return recyclerView.findChildViewUnder(e.x, e.y)?.let {
                    (recyclerView.getChildViewHolder(it) as? ViewHolder)?.getItemDetails()
                }
            }
        }

        inner class KeyProvider : ItemKeyProvider<String>(SCOPE_CACHED) {
            override fun getKey(position: Int): String {
                return getItem(position).uri.toString()
            }

            override fun getPosition(key: String): Int {
                return currentList.indexOfFirst { it.uri.toString() == key }
            }

        }

        private class ViewHolder(
            val binding: ListItemVacationImageBinding,
            onItemClick: (Int) -> Unit,
            onItemLongClick: (Int, View) -> Unit,
            onMenuIconClick: (Int, View) -> Unit
        ) : RecyclerView.ViewHolder(binding.root) {
            init {
                binding.root.setOnClickListener {
                    onItemClick(bindingAdapterPosition)
                }
                binding.root.setOnLongClickListener {
                    onItemLongClick(bindingAdapterPosition, it)
                    true
                }
                binding.menu.setOnClickListener {
                    onMenuIconClick(bindingAdapterPosition, it)
                }
            }

            fun getItemDetails() = object : ItemDetails<String>() {
                override fun getPosition(): Int = bindingAdapterPosition

                override fun getSelectionKey(): String? {
                    val adapter = bindingAdapter as? VacationImageAdapter
                    return adapter?.getItem(bindingAdapterPosition)?.uri?.toString()
                }

            }

        }

    }


}