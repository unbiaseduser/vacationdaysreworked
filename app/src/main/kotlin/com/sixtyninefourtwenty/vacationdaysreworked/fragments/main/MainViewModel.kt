package com.sixtyninefourtwenty.vacationdaysreworked.fragments.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory.Companion.APPLICATION_KEY
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.sixtyninefourtwenty.vacationdaysreworked.MyApplication
import com.sixtyninefourtwenty.vacationdaysreworked.data.Vacation
import com.sixtyninefourtwenty.vacationdaysreworked.data.repository.VacationsRepository
import kotlinx.coroutines.launch

class MainViewModel(private val vacationsRepository: VacationsRepository) : ViewModel() {

    fun getNumberOfVacations() = vacationsRepository.getNumberOfVacations()

    fun insertVacations(vararg vacations: Vacation) =
        viewModelScope.launch { vacationsRepository.insertVacation(*vacations) }

    fun updateVacations(vararg vacations: Vacation) =
        viewModelScope.launch { vacationsRepository.updateVacation(*vacations) }

    companion object {
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val repo = (get(APPLICATION_KEY) as MyApplication).repository
                MainViewModel(repo)
            }
        }
    }

}
