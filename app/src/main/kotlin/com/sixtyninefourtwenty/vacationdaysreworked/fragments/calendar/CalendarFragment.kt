package com.sixtyninefourtwenty.vacationdaysreworked.fragments.calendar

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.kizitonwose.calendar.core.CalendarDay
import com.kizitonwose.calendar.core.CalendarMonth
import com.kizitonwose.calendar.core.DayPosition
import com.kizitonwose.calendar.core.firstDayOfWeekFromLocale
import com.kizitonwose.calendar.view.MonthDayBinder
import com.kizitonwose.calendar.view.MonthHeaderFooterBinder
import com.kizitonwose.calendar.view.ViewContainer
import com.sixtyninefourtwenty.basefragments.ViewBindingFragment
import com.sixtyninefourtwenty.vacationdaysreworked.databinding.FragmentCalendarBinding
import com.sixtyninefourtwenty.vacationdaysreworked.databinding.SimpleCalendarDayBinding
import com.sixtyninefourtwenty.vacationdaysreworked.utils.obtainDeviceLocale
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.Month
import java.time.YearMonth
import java.time.format.DateTimeFormatter
import java.util.Locale

class CalendarFragment : ViewBindingFragment<FragmentCalendarBinding>(FragmentCalendarBinding::inflate) {

    private val calendarViewModel: CalendarViewModel by viewModels { CalendarViewModel.Factory }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val binding = requireBinding()
        val dayViewBinder = SimpleDayViewBinder()
        with(binding.calendar) {
            setup(
                startMonth = YearMonth.of(YearMonth.now().minusYears(100).year, Month.JANUARY),
                endMonth = YearMonth.of(YearMonth.now().plusYears(100).year, Month.DECEMBER),
                firstDayOfWeek = firstDayOfWeekFromLocale()
            )
            scrollToDate(LocalDate.now())
            dayBinder = dayViewBinder
            monthHeaderBinder = object : MonthHeaderFooterBinder<MonthViewContainer> {
                override fun bind(container: MonthViewContainer, data: CalendarMonth) {
                    container.textView.text = data.yearMonth.format(getYearMonthFormatter(requireContext()))
                }

                override fun create(view: View): MonthViewContainer = MonthViewContainer(view)
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                calendarViewModel.allVacationsByTimeFromAscending.collect { vacations ->
                    val dates = vacations.flatMap { it.dates }.distinct()
                    dayViewBinder.datesToHighlight = dates
                    binding.calendar.notifyCalendarChanged()
                }
            }
        }
    }

    private class SimpleDayViewContainer(view: View) : ViewContainer(view) {
        private val binding = SimpleCalendarDayBinding.bind(view)
        lateinit var day: CalendarDay
        val textView = binding.root
    }

    private class SimpleDayViewBinder : MonthDayBinder<SimpleDayViewContainer> {
        var datesToHighlight: List<LocalDate> = listOf()

        override fun bind(container: SimpleDayViewContainer, data: CalendarDay) {
            container.day = data
            container.textView.text = data.date.dayOfMonth.toString()
            if (data.position == DayPosition.OutDate || data.position == DayPosition.InDate) {
                container.textView.setTextColor(Color.TRANSPARENT)
            } else {
                if (datesToHighlight.contains(data.date)) {
                    container.textView.setTextColor(Color.RED)
                } else {
                    container.textView.setTextColor(Color.GRAY)
                }
            }
        }

        override fun create(view: View): SimpleDayViewContainer = SimpleDayViewContainer(view)
    }

    private class MonthViewContainer(view: View) : ViewContainer(view) {
        val textView = view as TextView
    }

    companion object {
        private const val YEAR_MONTH_PATTERN = "MMMM uuuu"
        private val yearMonthFormatterMap = mutableMapOf<Locale, DateTimeFormatter>()
        fun getYearMonthFormatter(context: Context): DateTimeFormatter {
            return yearMonthFormatterMap.computeIfAbsent(obtainDeviceLocale(context)) { l ->
                DateTimeFormatter.ofPattern(YEAR_MONTH_PATTERN, l)
            }
        }
    }

}