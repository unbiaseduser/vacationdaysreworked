package com.sixtyninefourtwenty.vacationdaysreworked.fragments

import android.os.Bundle
import androidx.preference.PreferenceCategory
import com.sixtyninefourtwenty.custompreferences.PreferenceFragmentCompatAccommodateCustomDialogPreferences
import com.sixtyninefourtwenty.custompreferences.installConfigurationChangePatch
import com.sixtyninefourtwenty.theming.preferences.addThemingPreferencesWithDefaultSettings
import com.sixtyninefourtwenty.vacationdaysreworked.R
import com.sixtyninefourtwenty.vacationdaysreworked.utils.myApplication

class SettingsFragment : PreferenceFragmentCompatAccommodateCustomDialogPreferences() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        installConfigurationChangePatch()
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
        with(findPreference<PreferenceCategory>("navigation_appearance_category")!!) {
            addThemingPreferencesWithDefaultSettings(
                requireActivity(),
                myApplication.prefs
            )
        }
    }

}