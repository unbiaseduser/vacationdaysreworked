package com.sixtyninefourtwenty.vacationdaysreworked.utils

import android.content.Context
import android.os.Build
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.sixtyninefourtwenty.vacationdaysreworked.MyApplication
import java.util.Locale

fun EditText.isBlank() = text == null || text.toString().isBlank()

fun EditText.getInput() = if (isBlank()) "" else text.toString().trim()

fun Fragment.navigate(navDirections: NavDirections) = findNavController().navigate(navDirections)

fun Fragment.makeToast(stringRes: Int) = Toast.makeText(requireContext(), stringRes, Toast.LENGTH_SHORT)

@Suppress("DEPRECATION")
fun obtainDeviceLocale(context: Context): Locale = AppCompatDelegate.getApplicationLocales()[0] ?: run {
    val systemLocale: Locale? = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        context.resources.configuration.locales[0]
    } else {
        context.resources.configuration.locale
    }

    systemLocale ?: Locale.getDefault()
}

val Context.myApplication: MyApplication get() = applicationContext as MyApplication

val Fragment.myApplication: MyApplication get() = requireContext().myApplication
