package com.sixtyninefourtwenty.vacationdaysreworked.utils

import android.net.Uri
import androidx.core.net.toUri
import androidx.room.TypeConverter

@Suppress("unused")
class Converters {

    @TypeConverter
    fun uriToString(uri: Uri?): String? = uri?.toString()

    @TypeConverter
    fun stringToUri(str: String?): Uri? = str?.toUri()

}