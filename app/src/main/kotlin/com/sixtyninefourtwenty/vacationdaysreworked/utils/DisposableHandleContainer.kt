package com.sixtyninefourtwenty.vacationdaysreworked.utils

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import kotlinx.coroutines.DisposableHandle
import java.util.Collections

class DisposableHandleContainer(private val lifecycle: Lifecycle) {

    constructor(lifecycleOwner: LifecycleOwner) : this(lifecycleOwner.lifecycle)

    private val disposableHandles: MutableCollection<DisposableHandle> = Collections.synchronizedSet(mutableSetOf())

    fun add(disposableHandle: DisposableHandle) {
        disposableHandles.add(disposableHandle)
    }

    fun clear() {
        disposableHandles.forEach { it.dispose() }
        disposableHandles.clear()
    }

    init {
        val state = lifecycle.currentState
        if (state != Lifecycle.State.DESTROYED && state.isAtLeast(Lifecycle.State.INITIALIZED)) {
            lifecycle.addObserver(object : LifecycleEventObserver {
                override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
                    if (lifecycle.currentState <= Lifecycle.State.DESTROYED) {
                        lifecycle.removeObserver(this)
                        clear()
                    }
                }
            })
        }
    }

}