package com.sixtyninefourtwenty.vacationdaysreworked

import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowInsetsControllerCompat
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.preference.PreferenceManager
import com.sixtyninefourtwenty.theming.applyTheming
import com.sixtyninefourtwenty.theming.preferences.copyFromDefaultThemingPreferences
import com.sixtyninefourtwenty.vacationdaysreworked.databinding.ActivityMainBinding
import com.sixtyninefourtwenty.vacationdaysreworked.utils.myApplication

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        PreferenceManager.setDefaultValues(this, R.xml.root_preferences, false)
        val prefs = myApplication.prefs
        if (!prefs.isThemingPreferencesMigrated) {
            copyFromDefaultThemingPreferences(
                this,
                copyMd3 = { prefs.md3 = it },
                copyThemeColor = { prefs.themeColor = it },
                copyLightDarkMode = { prefs.lightDarkMode = it },
                copyUseM3CustomColorThemeOnAndroid12 = { prefs.useM3CustomColorThemeOnAndroid12 = it }
            )
            prefs.isThemingPreferencesMigrated = true
        }
        applyTheming(
            material2ThemeStyleRes = R.style.Theme_VacationDaysReworked,
            material3CustomColorsThemeStyleRes = R.style.Theme_VacationDaysReworked_Material3_Android11,
            material3DynamicColorsThemeStyleRes = R.style.Theme_VacationDaysReworked_Material3,
            preferencesSupplier = prefs
        )
        super.onCreate(savedInstanceState)

        val w = window!!
        @Suppress("DEPRECATION")
        w.statusBarColor = Color.TRANSPARENT
        val isDarkMode = resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK == Configuration.UI_MODE_NIGHT_YES
        val wic = WindowInsetsControllerCompat(w, w.decorView)
        wic.isAppearanceLightStatusBars = !isDarkMode

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        appBarConfiguration = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController, appBarConfiguration)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }

}