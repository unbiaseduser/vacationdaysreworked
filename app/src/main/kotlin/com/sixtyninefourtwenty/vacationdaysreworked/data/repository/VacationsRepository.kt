package com.sixtyninefourtwenty.vacationdaysreworked.data.repository

import com.sixtyninefourtwenty.vacationdaysreworked.data.Vacation
import com.sixtyninefourtwenty.vacationdaysreworked.data.db.VacationsDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.withContext

class VacationsRepository(
    private val dao: VacationsDao,
    private val scope: CoroutineScope
) {

    fun getNumberOfVacations() = dao.getNumberOfVacations()

    val allVacationsByTimeFromAscending =
        dao.getAllVacationsByTimeFromAscending().distinctUntilChanged()

    suspend fun insertVacation(vararg vacations: Vacation) = withContext(scope.coroutineContext) {
        dao.insertVacation(*vacations)
    }

    suspend fun updateVacation(vararg vacations: Vacation) = withContext(scope.coroutineContext) {
        dao.updateVacation(*vacations)
    }

    suspend fun deleteVacation(vararg vacations: Vacation) = withContext(scope.coroutineContext) {
        dao.deleteVacation(*vacations)
    }

    suspend fun deleteVacation(vacations: List<Vacation>) = withContext(scope.coroutineContext) {
        dao.deleteVacation(vacations)
    }

    suspend fun findVacationsByName(name: String) = dao.findVacationsByName(name)

    suspend fun findVacationsByDescription(description: String) =
        dao.findVacationsByDescription(description)

    suspend fun checkIfVacationExists(name: String) =
        dao.checkIfVacationExists(name)

}
