package com.sixtyninefourtwenty.vacationdaysreworked.data.repository

import android.net.Uri
import com.sixtyninefourtwenty.vacationdaysreworked.data.Vacation
import com.sixtyninefourtwenty.vacationdaysreworked.data.VacationImage
import com.sixtyninefourtwenty.vacationdaysreworked.data.db.VacationImagesDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.withContext

class VacationImagesRepository(
    private val dao: VacationImagesDao,
    private val scope: CoroutineScope
) {

    suspend fun insertVacationImages(vararg images: VacationImage) = withContext(scope.coroutineContext) {
        dao.insertVacationImages(*images)
    }

    suspend fun updateVacationImages(vararg images: VacationImage) = withContext(scope.coroutineContext) {
        dao.updateVacationImages(*images)
    }

    suspend fun deleteVacationImages(vararg images: VacationImage) = withContext(scope.coroutineContext) {
        dao.deleteVacationImages(*images)
    }

    fun getImagesForVacation(vacation: Vacation) = dao.getImagesForVacation(vacation.name)

    suspend fun checkIfVacationImageExists(uri: Uri) = dao.checkIfVacationImageExists(uri.toString())

    suspend fun findImageByUri(uri: Uri) = dao.findImageByUri(uri.toString())

}