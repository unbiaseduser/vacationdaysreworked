package com.sixtyninefourtwenty.vacationdaysreworked.data

import android.net.Uri
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(
    tableName = "vacation_images",
    indices = [Index(
        value = ["vacation_name"]
    )],
    foreignKeys = [ForeignKey(
        entity = Vacation::class,
        parentColumns = arrayOf("name"),
        childColumns = arrayOf("vacation_name"),
        onDelete = ForeignKey.CASCADE
    )]
)
@Parcelize
data class VacationImage(
    @PrimaryKey val uri: Uri,
    @ColumnInfo(name = "vacation_name") val vacationName: String,
    val title: String
) : Parcelable
