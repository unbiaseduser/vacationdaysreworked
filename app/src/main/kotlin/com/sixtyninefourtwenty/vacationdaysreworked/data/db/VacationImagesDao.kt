package com.sixtyninefourtwenty.vacationdaysreworked.data.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.sixtyninefourtwenty.vacationdaysreworked.data.VacationImage
import kotlinx.coroutines.flow.Flow

@Dao
interface VacationImagesDao {

    @Insert
    suspend fun insertVacationImages(vararg images: VacationImage)

    @Update
    suspend fun updateVacationImages(vararg images: VacationImage)

    @Delete
    suspend fun deleteVacationImages(vararg images: VacationImage)

    @Query("SELECT * FROM vacation_images WHERE vacation_name = :vacationName")
    fun getImagesForVacation(vacationName: String): Flow<List<VacationImage>>

    @Query("SELECT EXISTS (SELECT uri FROM vacation_images WHERE uri = :uriString)")
    suspend fun checkIfVacationImageExists(uriString: String): Boolean

    @Query("SELECT * FROM vacation_images WHERE uri = :uriString LIMIT 1")
    suspend fun findImageByUri(uriString: String): VacationImage?

}