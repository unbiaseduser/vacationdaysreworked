package com.sixtyninefourtwenty.vacationdaysreworked.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.sixtyninefourtwenty.vacationdaysreworked.data.Vacation
import com.sixtyninefourtwenty.vacationdaysreworked.data.VacationImage
import com.sixtyninefourtwenty.vacationdaysreworked.utils.Converters

@Database(entities = [Vacation::class, VacationImage::class], version = 3)
@TypeConverters(Converters::class)
abstract class VacationsDatabase : RoomDatabase() {
    abstract fun vacationsDao(): VacationsDao
    abstract fun vacationImagesDao(): VacationImagesDao

    companion object {

        val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(db: SupportSQLiteDatabase) {
                db.execSQL("CREATE TABLE vacation_images (uri TEXT NOT NULL PRIMARY KEY, " +
                        "vacation_name TEXT NOT NULL, " +
                        "title TEXT NOT NULL, " +
                        "FOREIGN KEY (vacation_name) REFERENCES vacations(name) ON DELETE CASCADE)")
            }
        }

        val MIGRATION_2_3 = object : Migration(2, 3) {
            override fun migrate(db: SupportSQLiteDatabase) {
                db.execSQL("CREATE INDEX IF NOT EXISTS index_vacation_images_vacation_name ON vacation_images (vacation_name)")
            }
        }

        @Volatile
        private var INSTANCE: VacationsDatabase? = null

        fun getDatabase(context: Context): VacationsDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    VacationsDatabase::class.java,
                    "vacations"
                ).addMigrations(MIGRATION_1_2, MIGRATION_2_3).build()
                INSTANCE = instance
                return instance
            }
        }
    }

}