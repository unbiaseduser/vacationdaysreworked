package com.sixtyninefourtwenty.vacationdaysreworked.data.repository

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import com.sixtyninefourtwenty.theming.LightDarkMode
import com.sixtyninefourtwenty.theming.ThemeColor
import com.sixtyninefourtwenty.theming.preferences.ThemingPreferencesSupplier
import com.sixtyninefourtwenty.theming.preferences.toThemingPreferencesSupplier

class Preferences(context: Context): ThemingPreferencesSupplier {

    private val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    private val delegate = prefs.toThemingPreferencesSupplier(context)

    override var md3: Boolean by delegate::md3
    override var lightDarkMode: LightDarkMode by delegate::lightDarkMode
    override var themeColor: ThemeColor by delegate::themeColor
    override var useM3CustomColorThemeOnAndroid12: Boolean by delegate::useM3CustomColorThemeOnAndroid12

    var trackSickDays: Boolean
        get() = prefs.getBoolean("track_sick", true)
        set(value) { prefs.edit { putBoolean("track_sick", value) } }

    var isThemingPreferencesMigrated: Boolean
        get() = prefs.getBoolean("theming_preferences_migrated", false)
        set(value) { prefs.edit { putBoolean("theming_preferences_migrated", value) } }

}