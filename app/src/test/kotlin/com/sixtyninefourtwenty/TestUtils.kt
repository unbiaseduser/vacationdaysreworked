package com.sixtyninefourtwenty

import com.sixtyninefourtwenty.vacationdaysreworked.data.Vacation
import java.time.LocalDate
import java.time.LocalTime
import java.time.ZoneOffset

fun newTestVacation(
    dateFrom: LocalDate,
    dateTo: LocalDate
) = Vacation(
    "",
    "",
    false,
    dateFrom.atTime(LocalTime.of(0, 0)).toInstant(ZoneOffset.UTC).toEpochMilli(),
    dateTo.atTime(LocalTime.of(0, 0)).toInstant(ZoneOffset.UTC).toEpochMilli()
)
