# Vacation Days Remake
Remake of the [original Vacation Days](https://github.com/Crazy-Marvin/VacationDays), with new UI
and internal stuff.

## Features
- See how many vacation days you have for each month in a certain year
- Associate your personal images to each vacation (see disclaimer in app!)

## Roadmap
- Import/Export data
- Use a proper chart for displaying number-of-vacation-days statistics

## Releases
*TBD.*

## Disclaimer
- This app is not a fork of the original one, if you're curious.
- This app does ***not*** save copies of your images anywhere - it only creates a link between the
  location of the image and its associated vacation. No data leaves your device - ever.